package fr.afpa.beans;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name = "findById", query = "from Users u where u.idUser = :parametre")


public class Users {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_user")
	private int idUser;

	@NonNull
	private String nom;

	@NonNull
	private String prenom;

	@NonNull
	@Column(name = "mail_user")
	private String mail;

	@NonNull
	@Column(name = "librairie_user")
	private String nomLib;

	@NonNull
	private String tel;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_adresse")
	private Adresse adresse;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_compte")
	private Compte compte;
	
	@OneToMany(mappedBy = "user")
	private List<Annonce> listAnnonce;

	@Override
	public String toString() {
		return compte + "---------- VOS INFORMATIONS ----------\n\n           ID  : " + idUser + "\n           nom  : "
				+ nom + "\n           prenom : " + prenom + "\n           mail : " + mail + "\n           librairie : "
				+ nomLib + "\n           tel : " + tel + "\n\n" + adresse;
	}
	
	
	
}
