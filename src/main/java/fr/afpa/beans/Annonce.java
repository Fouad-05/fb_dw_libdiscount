package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@NamedQueries({
	@NamedQuery(name ="FindAnnonceByVille", query="FROM Annonce a where a.user.adresse.ville =:value"),
	@NamedQuery(name = "FindAnnonceByIsbn", query="FROM Annonce a where a.isbn =:value"),
	@NamedQuery(name = "FindAnnonceByMotcle", query="FROM Annonce a where a.titre LIKE :value"),
	@NamedQuery(name = "FindAnnonceByUser", query="FROM Annonce a where a.user.idUser =:value"),
	@NamedQuery(name = "findAnnonceByIdAnnonce", query="FROM Annonce a where a.idAnnonce =:value"),

})


public class Annonce {

	public Annonce( String titre, String isbn,  LocalDate dateEdition,
			 String maisonEdition,double prixUni,  int quantite,  double remise,
			Users user) {
		this.dateAnnonce = LocalDate.now();
		this.titre = titre;
		this.isbn = isbn;
		this.dateEdition = dateEdition;
		this.maisonEdition = maisonEdition;
		this.prixUni = prixUni;
		this.quantite = quantite;
		this.remise = remise;
		this.user = user;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idAnnonce;
	@NonNull
	private String titre;
	@NonNull
	private String isbn;
	@NonNull
	private LocalDate dateEdition;
	@NonNull
	private String maisonEdition;
	@NonNull
	private double prixUni;
	@NonNull
	private int quantite;
	@NonNull
	private double remise;
	@NonNull
	private LocalDate dateAnnonce;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_user")
	private Users user;
	
	@Override
	public String toString() {
		return "\n#------------------------------- "+ titre +" -------------------------------#\n"
			  +"           Numero Annonce :"+ idAnnonce +"                                   \n"	
			  +"           Isbn :"+ isbn +"                                                  \n"	
			  +"           DateEdition :"+ dateEdition +"                                    \n"	
			  +"           Maison Edition : "+ maisonEdition +"                              \n"	
			  +"           Prix unitaire  : "+ prixUni +"                                    \n"	
			  +"           Quantite  : "+ quantite +"                                        \n"	
			  +"           Remise  : "+ remise +"                                            \n"
			  +"           Date de l'annonce : "+ dateAnnonce+"                              \n";
		
	} 
	
	
	

}
