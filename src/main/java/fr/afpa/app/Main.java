package fr.afpa.app;

import java.time.LocalDate;
import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Users;
import fr.afpa.dao.DaoAnnonce;
import fr.afpa.utils.HibernateUtils;
import fr.afpa.vue.AffichageMenu;

//import fr.afpa.vue.AffichageMenu;

public class Main {

	public static void main(String[] args) {

		AffichageMenu af = new AffichageMenu();

		af.startMenu();
		
		Annonce ann1 = new Annonce("Vend HP","1-111-111-1",LocalDate.now().minusYears(50),"La Boutique",23,50,15,LocalDate.now());
		Users user = new Users("Doe","John", "johndoe@doe.fr", "La Lib", "0123456789");
		//Adresse adr = new Adresse("26","rue d'alsace","59180","Cappelle la grande");
		//Compte log = new Compte("tete","tete");
		
		ArrayList<Annonce> listAnnonce = new ArrayList<Annonce>();
		listAnnonce.add(ann1);
		user.setListAnnonce(listAnnonce);
		ann1.setUser(user);
		DaoAnnonce dao = new DaoAnnonce();
		dao.ajouterAnnonce(ann1);
		//dao.ajoutUser();
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();

	
		session.save(user);

		tx.commit();
		
		session.close();
		

	}

}
