package fr.afpa.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Users;
import fr.afpa.utils.HibernateUtils;

public class DaoAnnonce {
	private static Session session;
	

	/**
	 * Ajoute une annonce
	 * @param annonce
	 */
	public void ajouterAnnonce(Annonce annonce) {
		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.save(annonce);
		tx.commit();
		session.close();
		
	}

	/**
	 * Modifie une annonce
	 * @param annonce
	 */
	public void modifierAnnonce(Annonce annonce) {
		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.update(annonce);
		tx.commit();
		session.close();
	}

	
	/**
	 * Supprime une annonce
	 * @param annonce
	 */
	public void supprimeAnnonce(Annonce annonce) {
		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.delete(annonce);
		tx.commit();
		session.close();
	}

	
	/**
	 * 
	 * @param idUser
	 * @return idAnnonce
	 */
	public Annonce rechercherAnnonce(int idAnnonce) {	
		Query req = session.createNamedQuery("findAnnonceByIdUserAndIdAnnonce");
		req.setParameter("value", idAnnonce);
		Annonce annonce = (Annonce)req.getSingleResult();;
		return annonce;
	}

	/**
	 * Liste toutes les annonces
	 * 
	 * @return list<Annonce> : toutes les annonces
	 */
	public List<Annonce> listerAnnonce() {
		session = HibernateUtils.getSession();
		Query req = session.createQuery("from Annonce");
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;
	}
	

	/**
	 * Liste d'annonce de l'utilisateur
	 * 
	 * @param idUser
	 * @return list<Annonce>
	 */
	public List<Annonce> listerAnnonceParUser(int idUser) {	
		session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAnnonceByUser");
		req.setParameter("value", idUser);
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;

	}

	/**
	 * Recherche d'annonce par mot cl�
	 * 
	 * @param str
	 * @return list<Annonce>
	 */
	public List<Annonce> listeAnnonceParMotcle(String str) {

		session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAnnonceByMotcle");
		req.setParameter("value", "'%"+str +"%'");
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;

	}

	/**
	 * Recherche d'annonce avec le isbn
	 * 
	 * @param isbn
	 * @return list<Annonce>
	 */
	public List<Annonce> listeAnnonceParISBN(String isbn) {

		session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAnnonceByIsbn");
		req.setParameter("value", isbn);
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;

	}

	/**
	 * recherche d'annonce par ville
	 * 
	 * @param ville
	 * @return list<Annonce>
	 */
	public List<Annonce> listeAnnonceParVille(String ville) {
		session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAnnonceByVille");
		req.setParameter("value", ville);
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;
	}

}
