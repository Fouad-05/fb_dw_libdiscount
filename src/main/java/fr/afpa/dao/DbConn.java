package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConn {

	/**
	 * Function for create the connection with the base
	 * 
	 * @return Connection : a connection of the database
	 */
	public Connection connect() {

		try {

			String url = "jdbc:postgresql://localhost:5432/libDiscount";
			Properties props = new Properties();
			props.setProperty("user", "cda");
			props.setProperty("password", "cda");
			return DriverManager.getConnection(url, props);

		} catch (SQLException e) {

			e.printStackTrace();

		}

		return null;

	}

}
