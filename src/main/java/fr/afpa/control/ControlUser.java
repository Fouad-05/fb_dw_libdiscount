package fr.afpa.control;

import fr.afpa.metier.ServiceUser;

public class ControlUser {

	ServiceUser su = new ServiceUser(); 
	
	public void choixAjoutUser() {
		
		su.demandeAjoutUser();
		
	}
	
	public int choixAuthUser(String login, String mdp) {
		return su.demandeAuthUser(login, mdp);
	}

	public void demandeConsulterInfos(int idUser) {

		su.choixConsulterInfos(idUser);
		
	}

	public void demandeModifierUser(int idUser) {

		su.choixModifierUser(idUser);
		
	}

	public void demandeModifierMdp(int idUser) {

		su.choixModifierMdp(idUser);
		
	}

	public void demandeSupprimerUser(int idUser) {

		su.choixSupprimerUser(idUser);
		
	}

	/*public void ajouterUser() {
		su.demandeUser();
	}

	public void demandeModif(int userID, String demandeNom, String demandePrenom, String demandeMail, String demandeTel) {
		
		su.redirectionUpdateUser(userID, demandeNom, demandePrenom, demandeMail, demandeTel);
		
	}

	public void disabledUser(int idUser) {

		su.desUser(idUser);
		
	}
	
	public void voirInfos(int idUser) {

		su.conInfos(idUser);
		
	}*/
	
}
