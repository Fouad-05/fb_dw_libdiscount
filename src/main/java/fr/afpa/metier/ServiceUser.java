package fr.afpa.metier;

import java.util.Scanner;

import fr.afpa.control.ControlSaisie;
import fr.afpa.dao.DaoUser;

public class ServiceUser {

	private Scanner in = new Scanner(System.in);

	private DaoUser du = new DaoUser();

	private ControlSaisie controlS = new ControlSaisie();

	private String choice;

	private String demandeNom;
	private String demandePrenom;
	private String demandeMail;
	private String demandeTel;

	private String demandeNomLib;
	private String demandeNumRue;
	private String demandeNomRue;
	private String demandeCodeP;
	private String demandeVille;

	private String demandeLogin;
	private String demandeMdp;

	public int demandeAuthUser(String login, String mdp) {
		return du.authUser(login, mdp);
	}

	public void demandeAjoutUser() {

		System.out.print("Entrez le nom : ");
		demandeNom = in.nextLine();

		System.out.print("Entrez le prenom : ");

		demandePrenom = in.nextLine();

		System.out.print("Entrez le mail : ");
		demandeMail = in.nextLine();

		while (controlS.controlMail(demandeMail) != true) {

			System.out.print("Saisissez un mail contenant un '@' et un '.' ! : ");
			demandeMail = in.nextLine();

		}

		System.out.print("Entrez le num�ro de t�l : ");
		demandeTel = in.nextLine();

		while (controlS.controlTel(demandeTel) != true) {

			System.out.print("Saisissez un t�l�phone contenant 10 chiffres ! : ");
			demandeTel = in.nextLine();

		}

		System.out.print("Entrez le nom de la librairie : ");
		demandeNomLib = in.nextLine();

		System.out.print("Entrez le num�ro de rue de la librairie : ");
		demandeNumRue = in.nextLine();

		System.out.print("Entrez le nom de rue de la librairie : ");
		demandeNomRue = in.nextLine();

		System.out.print("Entrez le code postal de la librairie : ");
		demandeCodeP = in.nextLine();

		while (controlS.controlCodeP(demandeCodeP) != true) {

			System.out.print("Saisissez un code postal contenant 5 chiffres ! : ");
			demandeCodeP = in.nextLine();

		}

		System.out.print("Entrez la ville de la librairie : ");
		demandeVille = in.nextLine();
		demandeVille.toUpperCase();

		System.out.print("Entrez le login que vous souhaitez : ");
		demandeLogin = in.nextLine();

		System.out.print("Entrez le mot de passe que vous souhaitez : ");
		demandeMdp = in.nextLine();

		du.ajoutUser(demandeNumRue, demandeNomRue, demandeCodeP, demandeVille, demandeLogin, demandeMdp, demandeNom,
				demandePrenom, demandeMail, demandeNomLib, demandeTel);

	}

	public void choixConsulterInfos(int idUser) {

		du.consulterInfos(idUser);

	}

	public void choixModifierUser(int idUser) {

		System.out.print("Entrez le nom : ");
		demandeNom = in.nextLine();

		System.out.print("Entrez le prenom : ");

		demandePrenom = in.nextLine();

		System.out.print("Entrez le mail : ");
		demandeMail = in.nextLine();

		while (controlS.controlMail(demandeMail) != true) {

			System.out.print("Saisissez un mail contenant un '@' et un '.' ! : ");
			demandeMail = in.nextLine();

		}

		System.out.print("Entrez le num�ro de t�l : ");
		demandeTel = in.nextLine();

		while (controlS.controlTel(demandeTel) != true) {

			System.out.print("Saisissez un t�l�phone contenant 10 chiffres ! : ");
			demandeTel = in.nextLine();

		}

		System.out.println(
				"Avez-vous changer de librairie ? \n" + "1 - Entrez 'O' pour OUI\n" + "2 - Entrez 'N' pour NON\n");
		System.out.print("Votre choix : ");
		choice = in.nextLine();

		while ((!choice.equals("O")) && (!choice.equals("N"))) {

			System.out.println("Entrez un choix correct !" + "Avez-vous changer de librairie ?\n"
					+ "1 - Entrez 'O' pour OUI\n" + "2 - Entrez 'N' pour NON\n");
			System.out.print("Votre choix : ");
			choice = in.nextLine();

		}

		if (choice.equals("O")) {

			System.out.print("Entrez le nom de la librairie : ");
			demandeNomLib = in.nextLine();

			System.out.print("Entrez le num�ro de rue de la librairie : ");
			demandeNumRue = in.nextLine();

			System.out.print("Entrez le nom de rue de la librairie : ");
			demandeNomRue = in.nextLine();

			System.out.print("Entrez le code postal de la librairie : ");
			demandeCodeP = in.nextLine();

			while (controlS.controlCodeP(demandeCodeP) != true) {

				System.out.print("Saisissez un code postal contenant 5 chiffres ! : ");
				demandeCodeP = in.nextLine();

			}

			System.out.print("Entrez la ville de la librairie : ");
			demandeVille = in.nextLine();

			du.modifierInfosUser(idUser, choice, demandeNom, demandePrenom, demandeMail, demandeTel, demandeNomLib,
					demandeNumRue, demandeNomRue, demandeCodeP, demandeVille);
		}
	}

	public void choixModifierMdp(int idUser) {

		System.out.print("Quel est le nouveau mot de passe : ");
		demandeMdp = in.nextLine();

		du.modifierMdpUser(idUser, demandeMdp);

	}

	public void choixSupprimerUser(int idUser) {

		System.out.print("�tes-vous s�r de vouloir supprimer votre compte ? : \n" + "1 - Entrez 'O' pour OUI\n"
				+ "2 - Entrez 'N' pour NON\n");
		System.out.print("Votre choix : ");
		choice = in.nextLine();

		while ((!choice.equals("O")) && (!choice.equals("N"))) {

			System.out.println("Entrez un choix correct !" + "�tes-vous s�r de vouloir supprimer votre compte ? : \n"
					+ "1 - Entrez 'O' pour OUI\n" + "2 - Entrez 'N' pour NON\n");
			System.out.print("Votre choix : ");
			choice = in.nextLine();

		}

		if (choice.equals("O")) {

			du.supprimerUser(idUser);

		} else {

			System.out.println("Sage d�cision :)");

		}

	}

	/*
	 * public void redirectionUpdateUser(int userID, String nom, String prenom,
	 * String mail, String tel) {
	 * 
	 * //daoU.updateUser(userID, nom, prenom, mail, tel);
	 * 
	 * }
	 * 
	 *
	 * 
	 * public void desUser(int idUser) {
	 * 
	 * //daoU.isNotActive(idUser);
	 * 
	 * }**
	 * 
	 * public void conInfos(int idUser) {
	 * 
	 * daoU.recupAdresse(idUser); System.out.println(daoU.consulterInfos(idUser));
	 * 
	 * }
	 */
}
