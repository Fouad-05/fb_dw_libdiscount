package fr.afpa.metier;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Users;
import fr.afpa.dao.DaoAnnonce;
import fr.afpa.dao.DaoUser;

/**
 * 
 * @author Adrien , David (hibernate)
 *
 */
public class ServiceAnnonce {

	private DaoAnnonce daoA = new DaoAnnonce();
	private DaoUser daoU = new DaoUser();

	/**
	 * 
	 * Method for create a annonce , and store it in the db
	 * 
	 * @param titre
	 * @param isbn
	 * @param maisonEdition
	 * @param prixUni
	 * @param quantite
	 * @param remise
	 * @param dateEdition
	 * @param idUser
	 */
	public void addAnnonce(String titre, String isbn, String maisonEdition, double prixUni, int quantite, double remise,
			String dateEdition, int idUser) {
		
		daoU = new DaoUser();

		Annonce annonce = new Annonce();
		annonce.setTitre(titre);
		annonce.setIsbn(isbn);
		annonce.setMaisonEdition(maisonEdition);
		annonce.setDateEdition(LocalDate.parse(dateEdition, DateTimeFormatter.ofPattern("dd-MM-yyyy")));
		annonce.setQuantite(quantite);
		annonce.setRemise(remise);
		annonce.setPrixUni(prixUni);
//		Users user = daoU.rechercherUser(idUser);
//		if(user != null) {
//			annonce.setUser(user);
//		}
		calculPrixTotal(annonce);
		daoA.ajouterAnnonce(annonce);	

	}

	/**
	 * Method calcul the total price of the 'annonce'
	 * 
	 * @param annonce
	 */
	public double calculPrixTotal(Annonce annonce) {

		return (annonce.getPrixUni() * (1 - annonce.getRemise() / 100)) * annonce.getQuantite();

	}

	/**
	 * Method to list all annonce(s)
	 */
	public void listAnnonce() {

		List<Annonce> listAnnonce;

		listAnnonce = daoA.listerAnnonce();

		if (!listAnnonce.isEmpty()) {

			for (Annonce annonce : listAnnonce) {

				System.out.print(annonce);
				System.out.print("           Prix Total : " + calculPrixTotal(annonce) + " euros \n");
//				if (daoU.findUserById(annonce.getIdUser()) != null) {
//					
//					
//					System.out.print(daoU.findUserById(annonce.getIdUser()));
//					System.out.println(daoU.recupAdresse(annonce.getIdUser()));
//				}

			}

		} else {
			System.out.println("Aucune Annonce ");
		}

	}

	/**
	 * Method for search annonce by user id
	 * 
	 * @param idUser : the user id
	 */
	public void listAnnonceByUser(int idUser) {

		List<Annonce> listAnnonce;

		listAnnonce = daoA.listerAnnonceParUser(idUser);

		if (!listAnnonce.isEmpty()) {

			for (Annonce annonce : listAnnonce) {

				System.out.print(annonce);
				System.out.print("           Prix Total : " + calculPrixTotal(annonce) + " � \n");
			}
		} else {
			System.out.println("OUPS une erreure est survenue ... ");
		}

	}

	

	/**
	 * Method for search annonce by key word in the title
	 * 
	 * @param str : the key word
	 */
	public void listAnnonceByMotCle(String str) {

		List<Annonce> listAnnonce;

		listAnnonce = daoA.listeAnnonceParMotcle(str);

		if (!listAnnonce.isEmpty()) {

//			for (Annonce annonce : listAnnonce) {
//
//				System.out.print(annonce);
//				System.out.print("           Prix Total : " + calculPrixTotal(annonce) + " � \n");
//				if (daoU.findUserById(annonce.getIdUser()) != null) {
//
//					System.out.print(daoU.findUserById(annonce.getIdUser()));
//					System.out.println();
//
//				}
//
//			}

		} else {
			System.out.println("Aucune Annonce  ");
		}
	}

	/**
	 * Method for search annonce by isbn
	 * 
	 * @param isbn string : the isbn
	 */
	public void listAnnonceByISBN(String isbn) {

		List<Annonce> listAnnonce;

		listAnnonce = daoA.listeAnnonceParISBN(isbn);

		if (!listAnnonce.isEmpty()) {

//			for (Annonce annonce : listAnnonce) {
//
//				System.out.print(annonce);
//				System.out.print("           Prix Total : " + calculPrixTotal(annonce) + " euros \n");
//				if (daoU.findUserById(annonce.getIdUser()) != null) {
//
//					System.out.print(daoU.findUserById(annonce.getIdUser()));
//					System.out.println();
//				}
//
//			}

		} else {
			System.out.println("Aucune Annonce  ");
		}

	}

	/**
	 * Method for search annonce by ville
	 * 
	 * @param ville string : the ville
	 */
public void listAnnonceByVille(String ville) {

		List<Annonce> listAnnonce;

		listAnnonce = daoA.listeAnnonceParVille(ville);

//		if (!listAnnonce.isEmpty()) {
//
//			for (Annonce annonce : listAnnonce) {
//
//				System.out.print(annonce);
//				System.out.print("           Prix Total : " + calculPrixTotal(annonce) + " � \n");
//				if (daoU.findUserById(annonce.getIdUser()) != null) {
//
//					System.out.print(daoU.findUserById(annonce.getIdUser())+"\n");
//					System.out.println();
//
//				}
//
//			}
//
//		} else {
//			System.out.println("Aucune Annonce  ");
//		}
	}

	/**
	 * modifier une annonce
	 * @param annonce
	 */
	public void modifierAnnonce(Annonce annonce) {
		
		daoA.modifierAnnonce(annonce);
		
	}

	/**
	 * Rechercher une annonce avec l'id de l'utilisateur
	 * @param userID
	 * @param id : id of annonce
	 * @return
	 */
	public Annonce findAnnonceByIdUserAndIDAnnonce(int userID, int id) {
		
		return 	daoA.rechercherAnnonce(id);
	}

	/**
	 * Supprimer une annonce 
	 * @param annonce
	 */
	public void supprimerAnnonce(Annonce annonce) {
	
		daoA.supprimeAnnonce(annonce);
		
	}


}
